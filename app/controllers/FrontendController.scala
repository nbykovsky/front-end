package controllers

import javax.inject._
import play.api.Configuration
import play.api.http.{HttpEntity, HttpErrorHandler}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.ws._
import play.api.libs.ws.JsonBodyReadables._
import play.api.libs.ws.JsonBodyWritables._

import scala.concurrent.{ExecutionContext, Future}

/**
  * Frontend controller managing all static resource associate routes.
  * @param assets Assets controller reference.
  * @param cc Controller components reference.
  */
@Singleton
class FrontendController @Inject()(implicit ec: ExecutionContext, assets: Assets, errorHandler: HttpErrorHandler, config: Configuration, cc: ControllerComponents, ws: WSClient) extends AbstractController(cc) {

  def index: Action[AnyContent] = assets.at("index.html")

  def assetOrDefault(resource: String): Action[AnyContent] = if (resource.startsWith(config.get[String]("apiPrefix"))){
    Action.async(r => errorHandler.onClientError(r, NOT_FOUND, "Not found"))
  } else {
    if (resource.contains(".")) assets.at(resource) else index
  }


  /**
    * this transformation is used to convert WSResponse into Result
    */
  implicit def response2Result(response: Future[WSResponse]): Future[Result] = {
    response map {
      response =>
        val headers = response.headers
          .map { h => (h._1, h._2.head) }
          .filter { _._1.toLowerCase != "content-length" }
        Result(
          ResponseHeader(response.status, headers),
          HttpEntity.Strict(response.bodyAsBytes, Some(response.contentType))
        )
    }
  }


  def redirect(path: String): Action[AnyContent] = Action.async { request =>
    val urls = config.get[Map[String, String]]("dispatchUrl")
    path.split("/").toList match {
      case head::tail if urls.contains(head) =>
        val proxy = ws.url(s"${urls(head)}/${tail.mkString("/")}")
          .withMethod(request.method)
          .withHttpHeaders(request.headers.headers:_*)
          .withQueryStringParameters(request.queryString.mapValues(_.head).toSeq:_*)
          (request.body.asJson match {
            case None => proxy
            case Some(json) => proxy.withBody(json)
          }).execute()

      case _ => Future(NotFound(Json.obj("error"->"Unknown path")))
    }
  }

}
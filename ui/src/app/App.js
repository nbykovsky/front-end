import {withRouter} from "react-router";
import connect from "react-redux/es/connect/connect";
import LoadingScreen from "./common/LoadingScreen";
import {Route, Switch} from "react-router-dom";
import LoginPage from "./login";
import PrivateRoute from "./common/PrivateRoute";
import LandingPage from "./landing";
import React from "react";
import CssBaseline from "material-ui/CssBaseline";
import NotificationBar from "./common/NotificationBar";

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
};

export default  withRouter(connect(mapStateToProps)(({auth: {loading}}) => {

    const routes = (loading)?(
        <LoadingScreen/>
    ):(
        <Switch>
            <Route exact path='/login' component={LoginPage}/>
            <PrivateRoute exact path='/landing' requiredPrm={"admin"} component={LandingPage}/>
        </Switch>
    );

    return (
        <React.Fragment>
            <CssBaseline />
            {routes}
            <NotificationBar/>
        </React.Fragment>
    );

}));
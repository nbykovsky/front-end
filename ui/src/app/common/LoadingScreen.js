import React from 'react'
import ProgressCycle from './ProgressCycle'
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    screen: {
        height: '100vh',
    }
});

const LoadingSereen = props => {
    const {classes} = props;
    return (<Grid container className={classes.root} >
        <Grid item xs={12} >
            <Grid className={classes.screen} container justify="center" alignItems="center" >
                <ProgressCycle/>
            </Grid>
        </Grid>
    </Grid>)
};

export default withStyles(styles)(LoadingSereen)
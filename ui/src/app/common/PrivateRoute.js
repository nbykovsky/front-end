import connect from "react-redux/es/connect/connect";
import {Redirect, Route} from "react-router-dom";
import React from "react";

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps)(({component: Component, requiredPrm, auth:{prm}, ...rest}) => {
    return (<Route {...rest} render={(props) => {
        console.log("requiredPrm in prm", requiredPrm, prm);
        if(prm.includes(requiredPrm)) {
            return (<Component {...props} />)
        } else {
            console.log("redirecting");
            return (<Redirect to={{pathname:'/login', state:{requiredPrm, from: props.location}}} />)
        }
    }}/>)
});


export default pageName => {
    return {
        updateField: (path, value) => {
            return {
                type: `UPDATE_FIELD_${pageName}`,
                payload: {
                    path,
                    value
                }
            }
        }
    }

};

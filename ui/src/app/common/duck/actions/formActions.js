/**
 *
 */
export default pageName => {
    return {
        updatePath: (path, value) => {
            return {
                type: `UPDATE_${pageName}`,
                payload: {
                    path,
                    value
                }
            }
        },
        validError: ([path, message]) => {
            return {
                type: `VALIDATION_ERROR_${pageName}`,
                payload: {
                    path,
                    message
                }
            }
        },
        validOk: ([path,]) => {
            return {
                type: `VALIDATION_OK_${pageName}`,
                payload: {
                    path
                }
            }
        },
        fieldReset: ([path,]) => {
            return {
                type: `RESET_${pageName}`,
                payload: {
                    path
                }
            }
        }
    };
};



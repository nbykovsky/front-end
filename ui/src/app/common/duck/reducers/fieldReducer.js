import set from "lodash.set";

export default pageName => (state = {}, {type, payload}) => {
    const stateCopy = Object.assign({}, state);
    switch(type) {
        case `UPDATE_FIELD_${pageName}`: {
            const {path, value} = payload;
            set(stateCopy, `${path}`, value);
            break;
        }
        default :
    }
    return stateCopy;
};
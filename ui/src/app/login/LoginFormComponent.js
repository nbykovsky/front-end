/**
 * Simple login form with fields:
 * - email
 * - password
 *
 * and submit button
 */


import Grid from "material-ui/Grid";
import TextField from "material-ui/TextField";
import validators from "../../utils/validators";
import {FormControlLabel} from "material-ui/Form";
import Checkbox from "material-ui/Checkbox";
import Button from "material-ui/Button";
import React from "react";
import PropTypes from "prop-types";


const LoginForm = ({classes, logRegForm:{logForm: {email, password, rememberPassword}},loginAction, updatePath, validError, validOk, history}) => {
    return (
        <Grid item xs={10}>
            <TextField className={classes.textField}
                       fullWidth
                       label="Email"
                       type="email"
                       value={email.value}
                       onChange={(event, ) => updatePath('logForm.email', event.target.value)}
                       onBlur={() => validators.validateEmail('logForm.email', email.value).then(validOk).catch(validError)}
                       error={Boolean(email.error)}
                       helperText={email.error}
            /><br/>
            <TextField className={classes.textField}
                       fullWidth
                       label="Password"
                       type="password"
                       value={password.value}
                       onChange={(event, ) => updatePath('logForm.password', event.target.value)}
                       onBlur={() => validators.validateMessage('logForm.password', (password.value)?false:"Password must not be empty").then(validOk).catch(validError)}
                       error={Boolean(password.error)}
                       helperText={password.error}
            /><br/>
            <Grid container className={classes.checkBoxWrapper} justify="space-between" alignItems="center">
                <Grid item xs={12} sm={6}>
                    <FormControlLabel
                        control={
                            <Checkbox  className={classes.checkBox} checked={rememberPassword.value} onChange={(event, ) => updatePath('logForm.rememberPassword', event.target.checked)} color="primary"/>}
                        label="Remember me"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Button className={classes.buttonForgot} disabled size="small" color="primary" >Forgot password</Button>
                </Grid>
            </Grid>
            <Button size="large"
                    className={classes.buttonSubmit}
                    variant="raised"
                    color="primary"
                    onClick={() => {loginAction(email.value, password.value, history)}}
                    disabled={Boolean(!email.validated||!password.validated||email.error||password.error)}
            >Login</Button>
        </Grid>)
};

LoginForm.propTypes = {
    classes: PropTypes.object.isRequired,
    logRegForm: PropTypes.object.isRequired,
    loginAction: PropTypes.func.isRequired,
    updatePath: PropTypes.func.isRequired,
    validError: PropTypes.func.isRequired,
    validOk: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired
};

export default LoginForm
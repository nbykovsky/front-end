import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import LoginForm from "./LoginFormComponent";
import {withRouter} from "react-router";
import {updatePathLogRegFormAction, validErrorLogRegFormAction, validOkLogRegFormAction, loginAction} from "./duck";


const mapStateToProps = state => {
    return {
        logRegForm: state.logRegForm,
    }
};

const mapActionsToProps = (dispatch, ) => {
    return bindActionCreators({
        updatePath: updatePathLogRegFormAction,
        validError: validErrorLogRegFormAction,
        validOk: validOkLogRegFormAction,
        loginAction: loginAction,
    }, dispatch);
};

export default withRouter(connect(mapStateToProps, mapActionsToProps) (LoginForm))
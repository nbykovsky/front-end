/**
 * Login - Register form which combines login and registration form
 * form is being switched via tabs
 *
 */


import React from 'react'
import Paper from 'material-ui/Paper'
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import LoginForm from "./LoginFormContainer";
import RegisterForm from "./RegisterFormContainer";




const styles = theme=> ({
    root: {
        flexGrow: 1,
        //backgroundImage: 'url("https://static.lexpress.fr/medias_9559/w_2000,h_1125,c_fill,g_north/v1399453401/high-tech-ces-petits-francais-qui-cartonnent_4894463.jpg")',

    },
    screen: {
        height: '100vh',
    },
    paper: {
        minWidth: 360,
        minHeight: 290,
        textAlign: 'center',
        opacity: 0.95
    },
    textField: {
        marginTop: theme.spacing.unit,
    },
    buttonSubmit: {
        margin: theme.spacing.unit * 2,
        textTransform: 'none'
    },
    buttonForgot: {
        textTransform: 'none'
    },
    formWrapper: {
        margin: 'auto'
    },
    checkBoxWrapper: {
        marginTop: theme.spacing.unit,
    },
    checkBox: {
        margin: 0
    }
});


const LoginPaper = ({classes, logRegForm:{isLogin}, updatePath, fieldReset}) => {

    const form = (isLogin.value)?(<LoginForm classes={classes}/>):(<RegisterForm classes={classes}/>);

    const panel = (
        <Grid container className={classes.root} >
            <Grid item xs={12} >
                <Grid className={classes.screen} container justify="center" alignItems="center" >
                    <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
                        <Paper className={classes.paper} elevation={4}>
                            <AppBar position="static" color="default">
                                <Tabs value={isLogin.value?0:1} onChange={tabHelper} indicatorColor="primary" textColor="primary" fullWidth>
                                    <Tab label="Login" />
                                    <Tab label="Register" />
                                </Tabs>
                            </AppBar>
                            <Grid container style={{margin: 'auto'}}  justify="center">
                                {form}
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );

    function tabHelper(event, value) {
        updatePath('isLogin', value===0);
        if (value===0) fieldReset([['regForm.name', 'regForm.email', 'regForm.password', 'regForm.passwordConf']]); //If login tab, empty register tab
        fieldReset([['logForm.email', 'logForm.password']]) //if register, empty login tab
    }

    return (
        <div>
            {panel}
        </div>
    )
};

LoginPaper.propTypes = {
    classes: PropTypes.object.isRequired,
    logRegForm: PropTypes.shape({
        isLogin: PropTypes.object.isRequired
    }),
    updatePath: PropTypes.func.isRequired,
    fieldReset: PropTypes.func.isRequired
};


export default withStyles(styles)(LoginPaper);
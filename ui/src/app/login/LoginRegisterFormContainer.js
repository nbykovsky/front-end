import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import LoginPaper from "./LoginRegisterFormComponent"
import {fieldResetLogRegFormAction, updatePathLogRegFormAction} from "./duck";


const mapStateToProps = state => {
    return {
        logRegForm: state.logRegForm,
    }
};

const mapActionsToProps = (dispatch, ) => {
    return bindActionCreators({
        updatePath: updatePathLogRegFormAction,
        fieldReset: fieldResetLogRegFormAction,
    }, dispatch);
};

export default connect(mapStateToProps, mapActionsToProps) (LoginPaper);
/**
 * Simple registration form with fields:
 * - Name
 * - Email
 * - Password
 * - Password Confirmation
 *  and submit button
 */

import Grid from "material-ui/Grid";
import TextField from "material-ui/TextField";
import validators from "../../utils/validators";
import Button from "material-ui/Button";
import React from "react";
import PropTypes from "prop-types";


const RegisterForm = ({classes, logRegForm:{regForm: {name, email, password, passwordConf}}, registerAction, updatePath, validError, validOk}) => {
    return (
        <Grid item xs={10}>
            <TextField className={classes.textField}
                       fullWidth  label="Name"
                       type="text"
                       value={name.value}
                       onChange={(event, ) => updatePath('regForm.name', event.target.value)}
                       onBlur={() => validators.validateName('regForm.name', name.value).then(validOk).catch(validError)}
                       error={Boolean(name.error)}
                       helperText={name.error}
            /><br/>
            <TextField className={classes.textField}
                       fullWidth  label="Email"
                       type="email"
                       value={email.value}
                       onChange={(event, ) => updatePath('regForm.email', event.target.value)}
                       onBlur={() => validators.validateEmail('regForm.email', email.value).then(validOk).catch(validError)}
                       error={Boolean(email.error)}
                       helperText={email.error}
            /><br/>
            <TextField className={classes.textField}
                       fullWidth label="Password"
                       type="password"
                       value={password.value}
                       onChange={(event, ) => updatePath('regForm.password', event.target.value)}
                       onBlur={() => validators.validatePassword('regForm.password', password.value)
                           .then(([path, value]) => validators.validateMessage([path, 'regForm.passwordConf'], ((value!==passwordConf.value)&&(passwordConf.value))?"Passwords don't match":false))
                           .then(validOk).catch(validError)}
                       error={Boolean(password.error)}
                       helperText={password.error}
            /><br/>
            <TextField className={classes.textField}
                       fullWidth
                       label="Confirm Password"
                       type="password"
                       value={passwordConf.value}
                       onChange={(event, ) => updatePath('regForm.passwordConf', event.target.value)}
                       onBlur={() => validators.validatePassword('regForm.passwordConf',passwordConf.value)
                           .then(([path, value]) => validators.validateMessage([path, 'regForm.password'], ((value!==password.value)&&(password.value))?"Passwords don't match":false))
                           .then(validOk).catch(validError)}
                       error={Boolean(passwordConf.error)}
                       helperText={passwordConf.error}

            /><br/>
            <Button size="large"
                    className={classes.buttonSubmit}
                    variant="raised"
                    color="primary"
                    onClick={() => {registerAction(name.value, email.value, password.value)}}
                    disabled={Boolean(!email.validated||!password.validated||!passwordConf.validated||!name.validated||email.error||password.error||passwordConf.error||name.error)}
            >Register</Button>
        </Grid>)
};

RegisterForm.propTypes = {
    classes: PropTypes.object.isRequired,
    logRegForm: PropTypes.object.isRequired,
    registerAction: PropTypes.func.isRequired,
    updatePath: PropTypes.func.isRequired,
    validError: PropTypes.func.isRequired,
    validOk: PropTypes.func.isRequired,
};

export default RegisterForm;
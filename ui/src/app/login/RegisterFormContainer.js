import {bindActionCreators} from "redux";
import {updatePathLogRegFormAction, validErrorLogRegFormAction, validOkLogRegFormAction, registerAction} from "./duck";
import {withRouter} from "react-router";
import connect from "react-redux/es/connect/connect";
import RegisterForm from "./RegisterFormComponent";

const mapStateToProps = state => {
    return {
        logRegForm: state.logRegForm,
    }
};

const mapActionsToProps = (dispatch, ) => {
    return bindActionCreators({
        updatePath: updatePathLogRegFormAction,
        validError: validErrorLogRegFormAction,
        validOk: validOkLogRegFormAction,
        registerAction: registerAction,
    }, dispatch);
};


export default withRouter(connect(mapStateToProps, mapActionsToProps) (RegisterForm));
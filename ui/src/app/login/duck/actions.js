import FormActions from '../../common/duck/actions/formActions'
import FieldActions from '../../common/duck/actions/fieldActions'

const resetErrorAuthAction = () => {
    return {type: 'RESET_ERROR'}
};

const {
    updatePath: updatePathLogRegFormAction,
    validError: validErrorLogRegFormAction,
    validOk: validOkLogRegFormAction,
    fieldReset: fieldResetLogRegFormAction
} = FormActions('logRegForm');

const {updateField: updateFieldAuthAction} = FieldActions('auth');

const loginErrorAuthAction = error => {
    return {
        type: 'LOGIN_ERROR', payload: {error: error}
    }
};

const loginOkAuthAction = prm => {
    return {
        type: 'LOGIN_OK', payload: {prm}
    }
};

const logoutAuthAction = () => {
    return {type: 'LOGOUT'}
};

const registerOkAuthAction = msg => {
    return {type: 'REGISTER_OK', payload: {msg}}
};

const registerErrorAuthAction = error => {
    return {type: 'REGISTER_ERROR', payload: {error}}
};

export {
    resetErrorAuthAction,
    updatePathLogRegFormAction,
    validErrorLogRegFormAction,
    validOkLogRegFormAction,
    fieldResetLogRegFormAction,
    updateFieldAuthAction,
    loginErrorAuthAction,
    loginOkAuthAction,
    logoutAuthAction,
    registerOkAuthAction,
    registerErrorAuthAction}
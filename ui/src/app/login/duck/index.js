
export {AuthReducer, fieldReducer, logRegFormReducer, authFieldReducer} from './reducers';
export {
    registerAction,
    loginAction,
    authService,
    updatePathLogRegFormAction,
    resetErrorAuthAction,
    validErrorLogRegFormAction,
    validOkLogRegFormAction,
    fieldResetLogRegFormAction} from './operations';
// export default loginReducer;
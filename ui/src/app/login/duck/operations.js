// import {formManager, fieldManager, resetError} from "./actions"
import auth from "../../../backendapi/auth";
import storage from "../../../utils/storage";
import jwt_decode from "jwt-decode";
import {
    resetErrorAuthAction,
    updatePathLogRegFormAction,
    validErrorLogRegFormAction,
    validOkLogRegFormAction,
    fieldResetLogRegFormAction,
    updateFieldAuthAction,
    loginErrorAuthAction,
    loginOkAuthAction,
    logoutAuthAction,
    registerOkAuthAction,
    registerErrorAuthAction
} from "./actions"



/**
 * login service just fetches jwt and put it into the local storage
 */
//todo: this should be called a service
const loginAction = (login, password, history) => dispatch => {
    // setting screen to loading state
    dispatch(updateFieldAuthAction('loading', true));
    auth.login(login, password)
        .then(jwt => storage.putKey(jwt))
        //pushing to login and login will redirect to landing
        .then(()=>history.push('/login'))
        .catch((err) => dispatch(loginErrorAuthAction("User doesn't exists")))
        .finally(() => {
            dispatch(fieldResetLogRegFormAction([['logForm.email', 'logForm.password']]));
            dispatch(updateFieldAuthAction('loading', false))
        })
};

/**
 * auth service takes jwt from the local storage, validates it, extracts permissions and put it into redux store
 */
const authService = (history, requiredPrm, goingTo) => dispatch => {
    // dispatch(formManager('auth').updatePath('loading', true));
    //getting key from the local storage
    storage.getKey()
    //turning on loading mode
    //checking validity of the jwt
        .then(jwt => {
            dispatch(updateFieldAuthAction('loading', true));
            return auth.check(jwt);
        })
        //extracting permissions and putting those into redux store
        .then(jwt => {
            const {prm} = jwt_decode(jwt);
            if (prm.includes(requiredPrm)) {
                dispatch(loginOkAuthAction(prm))
            } else{
                throw Error("User doesn't have permissions for this page")
            }
        })
        //redirecting to the landing page
        .then(() => history.push(goingTo))
        //if something goes wrong, removing key from the storage and cleaning prm section of redux store
        .catch((error) => {storage.deleteKey().then(() => dispatch(logoutAuthAction()))})
        //finally turning off the loading mode
        .finally(() => dispatch(updateFieldAuthAction('loading', false)))

};

//todo: this also needs to be called a service
const registerAction = (name, login, password) => dispatch => {
    dispatch(updateFieldAuthAction('loading', true));
    auth.register(name, login, password)
        .then(msg => dispatch(registerOkAuthAction(msg)))
        .then(() => dispatch(updatePathLogRegFormAction('isLogin', true)))
        .catch(error => dispatch( registerErrorAuthAction(error)))
        .finally(() => {
            dispatch(updateFieldAuthAction('loading', false));
            dispatch(fieldResetLogRegFormAction([['regForm.name', 'regForm.email', 'regForm.password', 'regForm.passwordConf']]));
        })
};



export {
    registerAction,
    loginAction,
    authService,
    updatePathLogRegFormAction,
    resetErrorAuthAction,
    validErrorLogRegFormAction,
    validOkLogRegFormAction,
    fieldResetLogRegFormAction
};

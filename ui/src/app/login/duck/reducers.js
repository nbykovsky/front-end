import formReducer from '../../common/duck/reducers/formReducer'
import fieldReducer from '../../common/duck/reducers/fieldReducer'


const AuthReducer =  (state = {}, {type, payload}) => {
    const stateCopy = Object.assign({}, state);
    switch(type) {
        case 'LOGIN_OK': {
            const {prm} = payload;
            stateCopy.prm = prm;
            stateCopy.notify.message = "Success";
            stateCopy.notify.type = 'success';
            break;
        }
        case 'LOGOUT': {
            stateCopy.prm = [];
            break;
        }
        case 'LOGIN_ERROR': {
            const {error} = payload;
            stateCopy.notify.message = error;
            stateCopy.notify.type = 'error';
            break;
        }
        case 'REGISTER_OK': {
            stateCopy.notify.message = "Success";
            stateCopy.notify.type = 'success';
            break;
        }
        case 'REGISTER_ERROR': {
            const {error} = payload;
            stateCopy.notify.message =  error.response.data.error;
            stateCopy.notify.type = 'error';
            break;
        }
        case 'RESET_ERROR': {
            stateCopy.notify.message =  null;
            break
        }
        default:

    }
    return stateCopy
};

const logRegFormReducer = formReducer("logRegForm");

const authFieldReducer = fieldReducer("auth");

export {AuthReducer, logRegFormReducer, authFieldReducer}
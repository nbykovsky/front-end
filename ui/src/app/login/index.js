import React from 'react';
import LogRegForm from './LoginRegisterFormContainer';
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux';
import {authService} from './duck';
import {withRouter} from 'react-router'


class LoginPage extends React.Component {

    componentDidMount(){
        const {history, authService, location:{state}} = this.props;
        const goingTo = (state&&state.from&&state.from.pathname)?state.from.pathname:"/landing";
        const requiredPrmAdjusted = (state&&state.requiredPrm)?state.requiredPrm:"admin";
        authService(history, requiredPrmAdjusted, goingTo);
    }

    render(){
        return (<LogRegForm/>);
    }
}


const mapActionsToProps = (dispatch, ) => {
    return bindActionCreators({
        authService: authService
    }, dispatch);
};

export default withRouter(connect(()=>{return {}}, mapActionsToProps) (LoginPage));

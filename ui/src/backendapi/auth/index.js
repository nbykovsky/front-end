import axios from 'axios'


const login = (userId, password) => axios.post("/api/auth/login", {userId, password}).then(resp => resp.data.ok);


const register = (name, email, password) => axios.post("/api/auth/user", {name, email, password});

const check = jwt => {
    console.log("supposed jwt",jwt);
    return axios.get("/api/auth/check", {headers: {Authorization: jwt}})
        .then(resp => {
            if (resp.data.hasOwnProperty("ok")) return jwt;
            else throw Error("Error: token is no valid")
        });
};

export default {login, register, check}
import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from './reducers';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import {BrowserRouter } from 'react-router-dom'
import { routerMiddleware } from 'react-router-redux'
import {createBrowserHistory} from 'history';
import initState from "./utils/initState"
import App from './app/App'


const browserHistory = createBrowserHistory();
const routerMDW = routerMiddleware(browserHistory);

const allStoreEnhancers = compose(
    applyMiddleware(thunk),
    applyMiddleware(routerMDW),
    window.devToolsExtension && window.devToolsExtension()
);

const store = createStore(rootReducer, initState, allStoreEnhancers);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));

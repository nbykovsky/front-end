import {combineReducers} from "redux";
import reduceReducers from "reduce-reducers";
import {AuthReducer, authFieldReducer, logRegFormReducer} from "./app/login/duck";

export default combineReducers({
    logRegForm: logRegFormReducer,
    // landing: null,
    auth: reduceReducers(AuthReducer, authFieldReducer)
})
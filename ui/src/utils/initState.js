export default {
    /**
     * basically auth a global state which is used on all the pages
     */
    auth: {
        loading: false,
        notify: {
            type:null,
            message:null
        },
        prm: [],
    },
    logRegForm: {
        isLogin: {
            value:true
        },
        logForm: {
            email: {
                value: "",
                error: "",
                validated: false
            },
            password: {
                value: "",
                error: "",
                validated: false
            },
            rememberPassword: {
                value: false
            }
        },
        regForm: {
            name: {
                value: "",
                error: "",
                validated: false
            },
            email: {
                value: "",
                error: "",
                validated: false
            },
            password: {
                value: "",
                error: "",
                validated: false
            },
            passwordConf: {
                value: "",
                error: "",
                validated: false
            }
        }
    },
    // landing: {
    //     payload: undefined
    // }
};
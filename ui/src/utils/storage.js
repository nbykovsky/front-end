
const putKey = jwt => new Promise((resolve, reject) => {
    if(jwt){
        localStorage.setItem("jwt", jwt);
        resolve("ok");
    } else reject("Jwt is null on undefined");
});

const getKey = () => new Promise (
    (resolve, reject) => {
        const jwt = localStorage.getItem("jwt");
        jwt?resolve(jwt):reject("Jwt is not found");
    }
);

const deleteKey = () => new Promise(
    (resolve, ) => {
        localStorage.removeItem("jwt");
        resolve("ok")
    }
);

export default {putKey, getKey, deleteKey}
/**
 * Validates email using regex
 */
const validateEmail = (path, email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return new Promise((resolve, reject) => {
        const message = "Email is not valid";
        return re.test(String(email).toLowerCase())?resolve([path, email]):reject([path, message])
    });
};

/**
 * Validates length of password
 * todo: add more comprehensive validators
 */
const validatePassword = (path, password) => {
    return new Promise((resolve, reject) => {
        const message = "Password is too short";
        return (password.length > 2)?resolve([path, password]):reject([path, message])
    })
};

/**
 * Check whether errorMessage is empty
 */
const validateMessage = (path, errorMessage) => {
    return new Promise((resolve, reject) => {
        return (!errorMessage)?resolve([path, errorMessage]):reject([path, errorMessage])
    })
};

const validateName = (path, name) => {
    return new Promise((resolve, reject) => {
        return (name)?resolve([path, name]):reject([path, "Name is empty"])
    })
};

export default {validateEmail, validatePassword, validateMessage, validateName}